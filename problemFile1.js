const express = require('express')
const http = require('http');
const fs = require('fs');
const uuid = require('uuid');
const path = require('path');
const requestID = require('express-request-id');

const app = express();

const { port, statuscode } = require('./config');

middleWareLog = (request, response, next) => {                                                                                                      //creating a middleware for log file
    let log = {
        "URL": request.url,
        "request ID": request.id,
        "Date": new Date()
    };                                                                                                                                               //requesting url to store
    fs.appendFile('./logData.log',JSON.stringify(log,null,2), (err) => {                                                                            // creating and appending file 
        if (err) {
            next({});
        }
        else {
            console.log("Log added");    
            next();                                                                                                           //consoles if log added.
        }
    })
    
};

app.use(requestID());                                                                                                                               //calling request ID function
app.use(middleWareLog);                                                                                                                             //calling middleware


app.get('/log', (request, response, next) => {

    response.sendFile(path.join(__dirname, './logData.log')).end();
});
app.get('/html', (request, response, next) => {                                                                                                     //checking if url is an html

    response.send(`<!DOCTYPE html>
    <html>
      <head>
      </head>
      <body>
          <h1>Any fool can write code that a computer can understand. Good programmers write code that humans can understand.</h1>
          <p> - Martin Fowler</p>
   
      </body>
    </html>
   `).end();
});

app.get('/json', (request, response, next) => {                                                                                                     //checks if the url is of json type

    response.send({
        "slideshow": {
            "author": "Yours Truly",
            "date": "date of publication",
            "slides": [
                {
                    "title": "Wake up to WonderWidgets!",
                    "type": "all"
                },
                {
                    "items": [
                        "Why <em>WonderWidgets</em> are great",
                        "Who <em>buys</em> WonderWidgets"
                    ],
                    "title": "Overview",
                    "type": "all"
                }
            ],
            "title": "Sample Slide Show"
        }
    }
    ).end();
});
app.get('/uuid', (request, response, next) => {                                                                                                   //checks if the url is of uuid type

    let uuidNew = uuid.v4();                                                                                                                      //uuid module is used for using function v4().
    let finalOutput = ({ UUID: uuidNew });
    response.send(finalOutput).end();                                                                                                                   //ends response
});
app.get('/status/:statusCode', (request, response, next) => {
    let statuscodeFromUrl = parseInt(request.params.statusCode);                                                                                  //getting the url

    response.setHeader('Content-Type', 'application/json');
    if (isNaN(statuscodeFromUrl) === false) {
        if (statuscodeFromUrl in http.STATUS_CODES && statuscodeFromUrl != 100) {

            response.status(statuscodeFromUrl).send({ "Status_Code": statuscodeFromUrl }).end();                                                        //writing to the status code
        }
        else {

            response.status(400).send({ Status_Code: "Invalid: Bad request" }).end();                                                                   //writing to the status code
        }
    }
    else {

        response.status(400).send({ "Error ": "Wrong format for status input, use numeric value for the same. Retry! Viable format is /status/status-code" }).end();
    }
});
app.get('/delay/:delayTime', (request, response, next) => {                                                                                     //condition to check if url is of delay type
    let delaycodeFromUrl = parseInt(request.params.delayTime);

    if (delaycodeFromUrl >= 0 && isNaN(delaycodeFromUrl) === false) {
        setTimeout(() => {
            response.status(200);                                                                                                               //assigning statusCode as 200;environment variable used
            response.send({ "Wait Time ": + delaycodeFromUrl + " seconds." }).end();
        }, delaycodeFromUrl * 1000);                                                                                                            //defined delay in seconds
    }
    else {
        response.status(400).send({ "Error": "Invalid time input, please put positive time (in seconds) and it should be a numeric value. Retry! Viable format is /delay/delay-time" }).end();
    }
});
app.use((request, response, next) => {                                                                                                   //default output if no suiting URL found
    response.status(400).send(`<!DOCTYPE html>
    <html>
      <head>
      </head>
      <body>
          <h1>Input proper URL.</h1>
          <h3>Try using the following:</h3>
          <h5>/html</h5>
          <h5>/json</h5>
          <h5>/uuid</h5>
          <h5>/status/status code</h5>
          <h5>/delay/delay time in seconds</h5>
      </body>
    </html>
    `).end();
});

app.use((err,request,response,next) => {                                                                                            //error handling middleware
    
        console.log(err);
        const stat = err.statuscode || 500;                                                                                         //changes code to 500, when error comes.
        response.status(stat);
        response.json({"Error" : err.message}).end();
    
});

app.listen(port, (err) => {
    if (err) {
        console.error(err);
    }
    else {
        console.log("Server running");
    }
});